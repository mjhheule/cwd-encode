/************************************************************************
*  cwd-encode by Marijn Heule (October 15th, 2013)                      *
*  converts a graph formula in DIMACS format and a parameter k into     *
*  a CNF formula in DIMACS format that is satisfiable if and only if    *
*  corresponding graph has a clique-width less of equal to k            *
************************************************************************/

#include <stdio.h>
#include <stdlib.h>

//#define DIRECT
#define DERIVATION
#define CARDINALITY     // MUST BE ON WITH DERIVATION
//#define LINEAR

int **edge, **lookup;
int nColors, nVertices, nEdges, nGroups, nSteps;
int offset = 0, linear;

void addEdge (int vertexU, int vertexV) {
	if (!edge[ vertexU ][ vertexV ])
	edge[ vertexU ][ vertexV ] = edge[ vertexV ][ vertexU ] = ++nEdges; }

inline int getComponent (int vertexU, int vertexV, int time) {
	return nGroups*time + lookup[ vertexU ][ vertexV ]; }

inline int getGroup (int vertexU, int vertexV, int time) {
	return nGroups*(nSteps + 1 + time) + lookup[ vertexU ][ vertexV ]; }

#ifdef DIRECT
inline int getLabel (int vertex, int color, int time) {
	return 2 * nGroups * (nSteps+1) +
               nVertices * nColors * time + (vertex-1) * nColors + color; }
#else
#ifdef DERIVATION
inline int getRepresentative (int vertex, int time) {
	return 2 * nGroups * (nSteps+1) + nVertices * time + vertex; }

inline int getClique (int vertex, int color, int time) {
	return 2 * nGroups * (nSteps+1) + nVertices * (nSteps+1) +
               nVertices * (nColors-1) * time + (vertex-1) * (nColors-1) + color; }
#else
inline int getRepresentative (int vertex, int time) {
	return offset + nVertices * time + vertex; }

inline int getClique (int vertex, int color, int time) {
	return offset + nVertices * (nSteps+1) +
               nVertices * (nColors-1) * time + (vertex-1) * (nColors-1) + color; }
#endif
#endif

inline int getLinear (int vertex, int time) {
  return linear + time * nVertices + vertex; }

int main (int argc, char **argv) {
    int i, j, tmp;
    int u, v, w, x, t;
    int nLines, nVars, nClauses = 0;
    int vertexU, vertexV;
    int shorter = 0;

    if (argc < 3) { printf("wrong input: ./cwd-encode FILE COLORS\n"); exit(0); }
#ifndef DERIVATION
    if (argc > 3)
      offset = atoi  (argv[3]);
#else
    if (argc > 3)
      shorter = atoi  (argv[3]);
#endif

    FILE *file = fopen (argv[1], "r");
    nColors    = atoi  (argv[2]);

    do { tmp = fscanf (file, " p edge %i %i \n", &nVertices, &nLines);
         if (tmp > 0 && tmp != EOF) break; tmp = fscanf (file, "%*s\n"); }
    while (tmp != 2 && tmp != EOF);

    if (nColors == 1) {
      if (nLines == 0) printf("p cnf 0 0\n");
      else             printf("p cnf 0 1\n0\n");
      return 0;
    }

    nEdges    = 0;
    nSteps    = nVertices - nColors + 1 - shorter;
    printf("c shorter = %i (%i)\n", shorter, argc);

    edge = (int **) malloc (sizeof (int*) * (nVertices+1));
    for (u = 1; u <= nVertices; u++) edge[u] = (int *) malloc (sizeof (int) * (nVertices+1) );

    for (u = 1; u <= nVertices; u++)
      for (v = 1; v <= nVertices; v++)
        edge[u][v] = 0;

    for (i = 1; i <= nLines; i++) {
      tmp = fscanf (file, " e %i %i \n", &vertexU, &vertexV);
      if (vertexU < vertexV) addEdge (vertexU, vertexV);
      else		     addEdge (vertexV, vertexU); }

    lookup = (int **) malloc (sizeof (int*) * (nVertices+1));
    for (u = 1; u <= nVertices; u++) lookup[u] = (int *) malloc (sizeof (int) * (nVertices+1) );

    nGroups = 0;
    for (u = 1; u <= nVertices; u++)
	for (v = u + 1; v <= nVertices; v++)
	    lookup[u][v] = ++nGroups;

#ifdef CARDINALITY
#ifdef DIRECT
    nClauses += nSteps * nVertices * ((nColors*(nColors-1)/2)+1);     // label clauses
    nClauses += nSteps * nVertices * nColors * 3 * (nVertices-1) / 2; // relation clauses
#else
    nClauses += nSteps * nVertices * (nVertices+1) / 2;           // representative clauses
    nClauses += nSteps * nColors * nVertices * (nVertices-1) / 2; // clashing clique clauses
#endif
#endif
#ifdef DERIVATION
// counting neighborhood property clauses
    for (t = 1; t <= nSteps; t++)
        for (u=1; u <= nVertices; u++)
	    for (v=u+1; v <= nVertices; v++)
		if (edge[u][v])
		   for (w=1; w <= nVertices; w++) {
                      if ((u == w) || (v == w)) continue;
                      if (!edge[u][w]) nClauses++;
                      if (!edge[v][w]) nClauses++; }

// counting path property clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
          if (edge[u][v])
            for (w = 1; w <= nVertices; w++)
              if (edge[u][w] && (v != w))
                for (x = 1; x <= nVertices; x++)
                  if (edge[v][x] && (u != x) && !edge[w][x] && (w != x))
		    nClauses++;

    nClauses += nVertices * (nVertices-1); // component init and goal clauses
    nClauses += nSteps * nEdges + (nSteps+1) * nVertices * (nVertices-1) / 2; // supergroup clauses
    nClauses += nSteps * nVertices * (nVertices-1) / 2;
    nClauses += nSteps * nVertices * (nVertices-1) * (nVertices-2) / 2; // component clauses
    nClauses += nSteps * nVertices * (nVertices-1) / 2;
#ifndef DIRECT
    nClauses += nSteps * nVertices * (nVertices-1) * (nVertices-2) / 2; // group transitive clauses;
#endif
#endif
    nVars     = (nSteps + 1) * 2 * nGroups;
#ifdef CARDINALITY
    nVars    += (nSteps + 1) * nVertices * nColors;
#endif

#ifdef LINEAR
    linear = nVars;
    nVars += nSteps * nVertices;
    nClauses += nSteps * nVertices * (nVertices - 1) *  3 / 2;
#endif

    printf("c groups %i, colors %i\n", nGroups, nColors);
    printf("p cnf %i %i\n", nVars, nClauses);
#ifdef DERIVATION
// initialization and goal clauses
    for (u = 1; u <= nVertices; u++)
      for (v = u+1; v <= nVertices; v++)
        printf("-%i 0\n%i 0\n", getComponent (u, v, 0), getComponent (u, v, nSteps));

    for (t = 0; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
	  printf("%i -%i 0\n", getComponent (u, v, t), getGroup (u, v, t));

// edge property clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
	  if (edge[u][v]) printf("%i -%i 0\n", getComponent (u, v, t-1), getGroup (u, v, t));

// neighborhood property clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
          if (edge[u][v])
            for (w = 1; w <= nVertices; w++) {
              if ((u == w) || (v == w)) continue;
              if (!edge[u][w]) {
	        if (v < w) printf("%i -%i 0\n", getComponent (u, v, t-1), getGroup (v, w, t));
	        else       printf("%i -%i 0\n", getComponent (u, v, t-1), getGroup (w, v, t)); }
              if (!edge[v][w]) {
	        if (u < w) printf("%i -%i 0\n", getComponent (u, v, t-1), getGroup (u, w, t));
	        else       printf("%i -%i 0\n", getComponent (u, v, t-1), getGroup (w, u, t)); }
            }

// path property clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
          if (edge[u][v])
            for (w = 1; w <= nVertices; w++)
              if (edge[u][w] && (v != w))
                for (x = 1; x <= nVertices; x++)
                  if (edge[v][x] && (u != x) && !edge[w][x] && (w != x)) {
                    if (u < x) printf("%i -%i ", getComponent(u, v, t-1), getGroup (u, x, t));
                    else       printf("%i -%i ", getComponent(u, v, t-1), getGroup (x, u, t));
                    if (v < w) printf("-%i 0\n", getGroup (v, w, t));
                    else       printf("-%i 0\n", getGroup (w, v, t));
                  }

// component time clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
	  printf("-%i %i 0\n", getComponent (u, v, t-1), getComponent (u, v, t));

// group time clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
	  printf("-%i %i 0\n", getGroup (u, v, t-1), getGroup (u, v, t));

// component transitive clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
	  for (w = v+1; w <= nVertices; w++) {
	    printf("-%i -%i %i 0\n", getComponent (u, v, t), getComponent (u, w, t), getComponent (v, w, t));
	    printf("-%i -%i %i 0\n", getComponent (u, v, t), getComponent (v, w, t), getComponent (u, w, t));
	    printf("-%i -%i %i 0\n", getComponent (u, w, t), getComponent (v, w, t), getComponent (u, v, t));
          }
#endif
#ifdef DIRECT
// label clauses (nSteps+1) * nVertices * ((nColors*(nColors-1)/2)+1)
    for (t = 1; t <= nSteps; t++)
      for (v = 1; v <= nVertices; v++) {
        for (i = 1; i <= nColors; i++)
	  printf("%i ", getLabel (v, i, t));
        printf("0\n");

        for (i = 1; i <= nColors; i++)
	  for (j = i+1; j <= nColors; j++)
            printf("-%i -%i 0\n", getLabel (v, i, t), getLabel (v, j, t));
      }

// group relation clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
	  for (i = 1; i <= nColors; i++) {
            printf("-%i -%i -%i %i 0\n", getComponent (u, v, t), getLabel (u, i, t),
                                         getLabel (v, i, t), getGroup (u, v, t));
            printf("-%i -%i %i 0\n", getGroup (u, v, t), getLabel (u, i, t), getLabel (v, i, t));
            printf("-%i %i -%i 0\n", getGroup (u, v, t), getLabel (u, i, t), getLabel (v, i, t));
	  }
#else
#ifdef DERIVATION
// group transitive clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
	for (v = u+1; v <= nVertices; v++)
	  for (w = v+1; w <= nVertices; w++) {
            printf("-%i -%i %i 0\n", getGroup (u, v, t), getGroup (u, w, t), getGroup (v, w, t));
            printf("-%i -%i %i 0\n", getGroup (u, v, t), getGroup (v, w, t), getGroup (u, w, t));
            printf("-%i -%i %i 0\n", getGroup (u, w, t), getGroup (v, w, t), getGroup (u, v, t));
          }
#endif
#ifdef LINEAR
    for (t = 0; t < nSteps; t++)
      for (u = 1; u <= nVertices; u++)
        for (v = u + 1; v <= nVertices; v++) {
           printf("%i -%i -%i 0\n", getComponent (u,v,t), getLinear (u,t), getLinear (v,t));
           printf("-%i %i 0\n", getComponent (u,v,t), getLinear (u,t));
           printf("-%i %i 0\n", getComponent (u,v,t), getLinear (v,t)); }
#endif
#ifdef CARDINALITY
// representative clauses
    for (t = 1; t <= nSteps; t++)
      for (v = 1; v <= nVertices; v++) {
	for (u = 1; u < v; u++)
          printf("%i ", getGroup (u,v,t));
	printf("%i 0\n", getRepresentative (v,t)); }

    for (t = 1; t <= nSteps; t++)
      for (v = 1; v <= nVertices; v++)
	for (u = 1; u < v; u++)
	  printf("-%i -%i 0\n", getGroup (u, v, t), getRepresentative (v, t));

// clashing clique clauses
    for (t = 1; t <= nSteps; t++)
      for (u = 1; u <= nVertices; u++)
        for (v = u+1; v <= nVertices; v++) {
          printf("-%i -%i -%i -%i 0\n", getComponent (u, v, t), getRepresentative (u, t),
                            getRepresentative (v, t), getClique (u, nColors-1, t));
          for (i = 1; i < nColors-1; i++)
             printf("-%i -%i -%i %i -%i 0\n", getComponent (u, v, t), getRepresentative (u, t),
                            getRepresentative(v, t), getClique (v, i+1, t), getClique(u, i, t));
          printf("-%i -%i -%i %i 0\n", getComponent (u, v, t), getRepresentative (u, t),
                            getRepresentative (v, t), getClique (v, 1, t)); }
#endif
#endif
}
